import numpy as np
from pandas import Series, DataFrame
import pandas as pd
import matplotlib.pyplot as plt
from pylab import *
import os
from datetime import datetime
from datetime import timedelta
import matplotlib.dates as mdates
from df_subset2 import df_subset2

def pf_analysis(pi, return_df, bond_df, simulation_start, simulation_end, file_name='test'):
    ### read datas ###
    pos = pi
    pos.shape = (178, 1)
    ret = df_subset2(return_df, simulation_start, simulation_end)
    bond = df_subset2(bond_df, simulation_start, simulation_end)
    pos_mat = pos.T
    ret_mat = ret.values[:,1:]
    market_mat = ret.values[:, [1]]
    bond_mat = bond.values[:, 1:]
    total_pos = pos_mat.sum()
    print 'total position: ', total_pos
    bond_pos = 1. - total_pos
    print 'bond position: ', bond_pos
    bond_pos_mat = np.array([[bond_pos]])
    date_index = [datetime.strptime(x, '%m/%d/%y') for x in ret.date]
    date_index = [date_index[0] - timedelta(1)] + date_index

    ### concatenate position and return sequences ###
    posXret_mat = np.concatenate((pos_mat, ret_mat + 1))
    posXbond_mat = np.concatenate((bond_pos_mat, (bond_mat/100. + 1.)**(1./247.)))
    portfolio_value = np.zeros((posXret_mat.shape[0],))
    
    ### make portfolio index ###
    for i in range(178):
        portfolio_value += np.cumprod(posXret_mat[:, i])
    portfolio_value += np.cumprod(posXbond_mat)
    print portfolio_value

    ### make market index ###
    market_mat = np.concatenate((np.array([[1.]]), market_mat + 1))
    market_value = np.cumprod(market_mat)

    portfolio_value_ts = DataFrame(portfolio_value, index=date_index)
    portfolio_value_ts.columns = ['portfolio']
    market_value_ts = DataFrame(market_value, index=date_index)
    market_value_ts.columns = ['market']
    result_df = pd.concat([portfolio_value_ts, market_value_ts], axis=1)

    print type(portfolio_value_ts)
    portfolio_value_ts.to_csv('portfolio_value.csv')
    
    plt.clf()
    result_df.plot()
    plt.legend(loc='best')
    savefig(file_name + '.pdf', format='pdf')
    os.popen('open ' + file_name + '.pdf')
    return portfolio_value_ts, market_value_ts

def performance(ts, pf_name='portfolio', show=False):
    diff = ts.diff()
    diff.values[0] = ts.values[0] - 1.
    ts_lag = ts.shift(1)
    ts_lag.values[0] = 1.
    rts = diff/ts_lag
    mean = rts.mean()
    std = rts.std()
    a_rt = (1 + mean) ** 250. - 1
    Sharpe = sqrt(250) * mean / std
    result_df = DataFrame([mean, std, a_rt, Sharpe], index=['daily return', 'standard deviation', 'annual return', 'Sharpe ratio'], columns=[pf_name])
    if show: print result_df

    return result_df

if __name__ == '__main__':
    pf_value, mkt_value = pf_analysis()
    performance(pf_value)
    performance(mkt_value)
