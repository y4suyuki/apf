# alpha portfolio simulator

##Usage
```sh
% python main_apf3.py
```
then program ask
```sh
% Enter risk estimation start
```
so enter the date of risk estimation start with formating 'yyyy-mm-dd'.
you also need 

+ risk estimation end
+ alpha estimation start
+ alpha estimation end

and program ask 
```sh
% Select alpha estimation method: mean or regression
```
You need enter 'regression' here because 'mean' is not work for naive case.
 
Finally, you enter simulation start and simulation end date and program simulate
the performace of market, naive and alpha portfolio then show a chart.