import numpy as np
from pandas import Series, DataFrame
import pandas as pd
from scipy.optimize import curve_fit
from pylab import *
import pylab as pl

def regre(x, y):
    def func(x, a, b):
        return a * x + b
    popt, pcov = curve_fit(func, x, y)
    return popt, pcov

def capm(market_df, bond_df, stock_df):
    coef, cov = regre(market_df, stock_df)
    beta = coef[0]; alpha = coef[1]
    print 'slope:', beta, 'intercept:', alpha
    fit = beta * (market_df) + alpha
    title('Linear regression: return = beta * (market - bond) + bond')
    plot(market_df, stock_df, 'go')
    plot(market_df, fit, 'r--')
    legend(['data', 'regression'])

    show()
    return beta, alpha

def simple_reg(df1, df2, n, plot_fig =False):
    coef, cov = regre(df1, df2)
    beta = coef[0]; alpha = coef[1]
    print n,'beta:', beta, 'alpha', alpha
    if plot_fig:
        fit = beta * (df1.values) + alpha
        title('Linear regression stock#' + str(n))
        plot(df1.values, df2.values, 'go')
        plot(df1.values, fit, 'r--')
        legend(['data', 'regression'])
        pl.savefig('regression' + str(n) + '.pdf', format='pdf')
        pl.close()
    return beta, alpha
    
class Capm(object):
    def __init__(self, market_df, bond_df, stock_df):
        self.coef, self.cov = regre(market_df - bond_df, stock_df - bond_df)
        self.beta, self.alpha = self.coef

def mk_testdata(n):
    return_df = pd.read_csv('subset.csv')
    bond_df = pd.read_csv('bond_subset.csv')
    mdf = return_df.market.values
    bdf = bond_df.bond.values
    sdf = return_df[str(n)].values
    
    return mdf, bdf, sdf

def capm_reg(n):
    m, d, s = mk_testdata(n)
    capm(m, d, s)
