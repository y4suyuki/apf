import numpy as np
from pandas import Series, DataFrame
import pandas as pd
from sys import argv
from numpy import linalg as LA
from df_subset2 import df_subset2
from alph_reg_mktrev import main_alph_reg

def make_weight(sigma, P, A, exp_alpha):
    pi = np.dot(np.dot(np.dot(np.dot(LA.inv(sigma.T), P), LA.inv(A)), P.T), exp_alpha)
    return pi

def make_return_df(return_file):
    names = ['date', 'market']
    securities_number = [x for x in range(1, 178)]
    names = names + securities_number
    return_df = pd.read_csv(return_file, names=names)
    return return_df

def make_sigma(return_df, bond_df):
    # compute betas
    beta_list = [1]
    for i in range(1, 178):
        beta = return_df[['market', i]].cov()['market'][1] / return_df[['market', i]].cov()['market'][0]
        beta_list.append(beta)

    # compute std of residuals
    
    res_std_list = [return_df.market.std()]
    for i in range(1, 178):
        res = return_df[i] - bond_df.bond - beta_list[i] * (return_df.market - bond_df.bond)
        res_std_list.append(res.std())

    sigma = np.zeros((178, 178))
    for i in range(178):
        sigma[i, 0] = beta_list[i]

    for i in range(1, 178):
        for j in range(1, 178):
            if i == j:
                sigma[i, j] = res_std_list[i]
    for i in range(178):
        sigma[i, 0] = sigma[i, 0] * res_std_list[i]
    DataFrame(sigma).to_csv('sigma2.csv', index=False)
    return sigma
        

def make_theta(sigma, return_df, bond_df):
    return_bond = [return_df.market - bond_df.bond]
    for i in range(1, 178):
        return_bond.append(return_df[i] - bond_df.bond)

    return_bond_df = pd.concat(return_bond, axis=1)
    theta = np.dot(LA.inv(sigma), return_bond_df.values.T)
    theta_df =DataFrame(theta.T)
    theta_df.to_csv('theta2.csv', index=False)
    return theta_df

def make_A(theta_df, a, T=247):
    As = []
    eigs = LA.eig(theta_df.cov())[0]
    for e in eigs:
        As.append(a - (1 - a) * e * T)
    A_mat = np.zeros((178, 178))
    for i in range(178):
        for j in range(178):
            if i == j:
                A_mat[i, j] = As[i]
    DataFrame(A_mat).to_csv('A2.csv', index=False)
    return A_mat

def mk_exp_theta(exp_alpha, sigma, bond_df):
    risk = []
    for i in range(178):
        for j in range(178):
            if i == j:
                risk.append(sigma[i,j])
    risk_df = DataFrame(risk)
    risk_df.to_csv('risk2.csv', index=False)
    
    exp_alpha.values[0] = exp_alpha.values[0] - float(bond_df.bond.mean())
    exp_theta = exp_alpha.values / risk_df.values
    DataFrame(exp_theta).to_csv('exp_theta.csv', index=False)
    return exp_theta


def risk_est(return_df, bond_df, risk_estimation_start, risk_estimation_end):
        
    ### risk estimation ###

    risk_rt_df = df_subset2(return_df, risk_estimation_start, risk_estimation_end)
    risk_bd_df = df_subset2(bond_df, risk_estimation_start, risk_estimation_end)
    sigma = make_sigma(risk_rt_df, risk_bd_df)
    theta_df = make_theta(sigma, risk_rt_df, risk_bd_df)
    theta_cov_df = theta_df.cov()
    theta_cov_df.to_csv('theta_cov.csv', index=False)
    a = float(input('value of a:\n'))
    A = make_A(theta_df, a)
    P = LA.eig(theta_cov_df)[1]
    DataFrame(P).to_csv('P.csv', index=False)
    return sigma, A, P

def alph_est(return_df, bond_df, alph_estimation_start, alph_estimation_end):

    ### naive alpha estimation ###

    alph_rt_df = df_subset2(return_df, alph_estimation_start, alph_estimation_end)
    alph_bd_df = df_subset2(bond_df, alph_estimation_start, alph_estimation_end)
    ar_df = pd.read_csv('a_ratings_ffill.csv', names=['date']+[x for x in range(1, 178)])
    ar_df_sub = df_subset2(ar_df, alph_estimation_start, alph_estimation_end)
    last_date = ar_df_sub.index[len(ar_df_sub.index) - 1]
    naive_alph = ar_df_sub.ix[last_date][1:178]
    m0 = Series([alph_rt_df.market.mean()])
    
    alph_sigma = make_sigma(alph_rt_df, alph_bd_df)
    
    main_alph_reg(alph_estimation_start, alph_estimation_end)
    exp_alpha = pd.read_csv('exp_alph.csv', names=['alpha'])
    naive_alpha = naive_alph / (naive_alph.max() / exp_alpha[1:178].max().values)
    naive_alpha = pd.concat([m0, naive_alpha])
    naive_alpha_df = DataFrame(naive_alpha.values)
    exp_theta = mk_exp_theta(naive_alpha_df, alph_sigma, alph_bd_df)

    return exp_theta

def mk_apf(return_file, bond_file):
    ### init ###
    print 'Loading ' + return_file + ' and ' + bond_file + '.'
    return_df = make_return_df(return_file)
    bond_df = pd.read_csv(bond_file, names=['date', 'bond'])
    bond_df.bond = bond_df.bond / 100 / 250   # convert bond to daily data


    ### risk estimation ###
    risk_start = input('Enter risk estimation start\n')
    risk_end = input('Enter risk estimation end\n')
    print 'Computing sigma, A, P...'
    sigma, A, P = risk_est(return_df, bond_df, risk_start, risk_end)
    

    ### alpha estimation ###
    alph_start = input('Enter alpha estimation start\n')
    alph_end = input('Enter alpha estimation end\n')
    exp_theta = alph_est(return_df, bond_df, alph_start, alph_end)

    for_file_name = 'risk' + risk_start + '~' + risk_end + 'alph' + alph_start + '~' + alph_end
    pi = make_weight(sigma, P, A, exp_theta)
    return pi, return_df, bond_df, for_file_name
