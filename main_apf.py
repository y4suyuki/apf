#!/bin/local/python
from pandas import DataFrame
import pandas as pd
from apf2 import mk_apf 
from pf_analysis2 import pf_analysis, performance

pi, return_df, bond_df, for_file_name = mk_apf('return_data.csv', 'bond_data.csv')
print pi
DataFrame(pi).to_csv('pi.csv', index=False)

print 'Select simulation time'
sim_start = input('Enter simulation start date\n')
sim_end = input('Enter simulation end date\n')

file_name = for_file_name + 'sim' + sim_start + '~' + sim_end
print 'Computing portfolio performance'
pf_value, mkt_value = pf_analysis(pi, return_df, bond_df, sim_start, sim_end, file_name)
pf_result_df = performance(pf_value, 'portfolio')
mkt_result_df = performance(mkt_value, 'market')
result_df = pd.concat([pf_result_df, mkt_result_df], axis=1)
print result_df
result_df.to_csv(file_name + '.csv')
