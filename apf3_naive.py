import numpy as np
from pandas import Series, DataFrame
import pandas as pd
from numpy import linalg as LA
from alph_reg_mktrev import main_alph_reg
from file_io import mk_return_df, mk_bond_df, mk_ar_df

def make_weight(sigma, P, A, exp_alpha):
    pi = np.dot(np.dot(np.dot(np.dot(LA.inv(sigma.T), P), LA.inv(A)), P.T), exp_alpha)
    return pi

def make_sigma(return_df, bond_df):
    # compute betas
    beta_list = [1]
    for i in range(1, 178):
        beta = return_df[['market', i]].cov()['market'][1] / return_df[['market', i]].cov()['market'][0]
        beta_list.append(beta)

    # compute std of residuals
    res_std_list = [return_df.market.std()]
    for i in range(1, 178):
        res = return_df[i] - bond_df.bond - beta_list[i] * (return_df.market - bond_df.bond)
        res_std_list.append(res.std())

    sigma = np.zeros((178, 178))
    for i in range(178):
        if i >= 1:
            sigma[i, i] = res_std_list[i]
        sigma[i, 0] = beta_list[i] * res_std_list[i]

    DataFrame(sigma).to_csv('sigma2.csv', index=False)
    return sigma
        

def make_theta(sigma, return_df, bond_df):
    return_bond = return_df[['market'] + range(1, len(return_df.columns) - 1)] - bond_df.bond
    theta = np.dot(LA.inv(sigma), return_bond.values.T)
    theta_df = DataFrame(theta.T)
    theta_df.to_csv('theta2.csv', index=False)
    return theta_df

def make_A(theta_df, a, T=247):

    eigs = LA.eig(theta_df.cov())[0]
    As = map(lambda e: a - (1 - a) * e * T, eigs)
    A_mat = np.zeros((178, 178))

    for i in range(178):
        A_mat[i, i] = As[i]

    DataFrame(A_mat).to_csv('A2.csv', index=False)
    return A_mat

def mk_exp_theta(exp_alpha, sigma, bond_df):
    risk = map(lambda i: sigma[i,i], range(178))
    risk_df = DataFrame(risk)
    risk_df.to_csv('risk2.csv', index=False)
    
    exp_alpha.values[0] = exp_alpha.values[0] - float(bond_df.bond.mean())
    exp_theta = exp_alpha.values / risk_df.values
    DataFrame(exp_theta).to_csv('exp_theta.csv', index=False)
    return exp_theta


def risk_est(return_df, bond_df, risk_estimation_start, risk_estimation_end):
        
    ### risk estimation ###

    risk_rt_df = return_df.ix[ risk_estimation_start: risk_estimation_end ]
    risk_bd_df =   bond_df.ix[ risk_estimation_start: risk_estimation_end ]
    sigma = make_sigma(risk_rt_df, risk_bd_df)
    theta_df = make_theta(sigma, risk_rt_df, risk_bd_df)
    theta_cov_df = theta_df.cov()
    theta_cov_df.to_csv('theta_cov.csv', index=False)
    a = float(input('value of a:\n'))
    A = make_A(theta_df, a)
    P = LA.eig(theta_cov_df)[1]
    DataFrame(P).to_csv('P.csv', index=False)
    return sigma, A, P

def alph_est(return_df, bond_df, alph_estimation_start, alph_estimation_end, alph):
    
    ### alpha estimation ###

    alph_rt_df = return_df.ix[ alph_estimation_start: alph_estimation_end ]
    alph_bd_df =   bond_df.ix[ alph_estimation_start: alph_estimation_end ]

    alph_sigma = make_sigma(alph_rt_df, alph_bd_df)
    if alph == 'mean':
        alph_theta_df = make_theta(alph_sigma, alph_rt_df, alph_bd_df)
        exp_theta = alph_theta_df.mean()
        return exp_theta
    elif alph == 'regression':
        ar_df = mk_ar_df('a_ratings_ffill.csv')
        main_alph_reg(return_df, bond_df, ar_df, alph_estimation_start, alph_estimation_end)
        exp_alpha = pd.read_csv('exp_alph.csv', names=['alpha'])
        exp_theta = mk_exp_theta(exp_alpha, alph_sigma, alph_bd_df)
        
        """ for naive alpha estimation """

        ar_df_sub = ar_df.ix[ alph_estimation_start: alph_estimation_end]
        last_date = ar_df_sub.index[len(ar_df_sub.index) - 1]
        naive_alph = ar_df_sub.ix[last_date][1:178]
        m0 = Series([alph_rt_df.market.mean()])
    
        naive_alpha = naive_alph / (naive_alph.max() / exp_alpha[1:178].max().values)
        naive_alpha = pd.concat([m0, naive_alpha])
        naive_alpha_df = DataFrame(naive_alpha.values)
        naive_exp_theta = mk_exp_theta(naive_alpha_df, alph_sigma, alph_bd_df)
        return exp_theta, naive_exp_theta

def mk_apf(return_df, bond_df):

    ### risk estimation ###
    risk_start = input('Enter risk estimation start yyyy-mm-dd\n')
    risk_end = input('Enter risk estimation end yyyy-mm-dd\n')
    print 'Computing sigma, A, P...'
    sigma, A, P = risk_est(return_df, bond_df, risk_start, risk_end)

    ### alpha estimation ###
    alph_start = input('Enter alpha estimation start yyyy-mm-dd\n')
    alph_end = input('Enter alpha estimation end yyyy-mm-dd\n')
    alph = input('Select alpha estimation method: mean or regression\n')
    exp_theta, naive_exp_theta = alph_est(return_df, bond_df, alph_start, alph_end, alph)

    for_file_name = 'risk' + risk_start + '~' + risk_end + 'alph' + alph_start + '~' + alph_end + alph
    pi = make_weight(sigma, P, A, exp_theta)
    naive_pi = make_weight(sigma, P, A, naive_exp_theta)
    return pi, for_file_name, naive_pi
