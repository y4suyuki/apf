import pandas as pd
from datetime import datetime as dt


def date_parser(x):
    return dt.strptime(x, '%m/%d/%y')

def mk_return_df(return_file):
    names = ['date', 'market']
    ret_df = pd.read_csv(return_file, header=None)
    names = names + range(1, len(ret_df.columns) - 1)
    ret_df.columns = names
    date_index = map(date_parser, ret_df.date)
    ret_df.index = date_index
    return ret_df

def mk_bond_df(bond_file):
    b_df = pd.read_csv(bond_file, names=['date', 'bond'])
    date_index = map(date_parser, b_df.date)
    b_df.index = date_index
    return b_df

def mk_ar_df(ar_file):
    ar_df = pd.read_csv(ar_file, header=None)
    names = ['date'] + range(1, len(ar_df.columns))
    ar_df.columns = names
    date_index = map(date_parser, ar_df.date)
    ar_df.index = date_index
    return ar_df
