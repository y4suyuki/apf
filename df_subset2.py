import numpy as np
from pandas import Series, DataFrame
import pandas as pd
from datetime import datetime

def df_subset2(df, start, end):
    date_index = [datetime.strptime(x, '%m/%d/%y') for x in df.date]
    df.index = date_index
    df_subseted = df.ix[start: end]
    return df_subseted

def mk_return_df(return_file):
    names = ['date', 'market']
    sec_num = [x for x in range(1, 178)]
    names = names + sec_num
    return_df = pd.read_csv(return_file, names=names)
    return return_df
