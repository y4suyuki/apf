import numpy as np
from pandas import Series, DataFrame
import pandas as pd
from df_subset2 import df_subset2, mk_return_df
import datetime as dt
from datetime import datetime
from regression import simple_reg
from file_io import mk_return_df, mk_bond_df

def mk_beta(rdf, n):
    beta = rdf[['market', n]].cov()['market'][1] / rdf[['market', n]].cov()['market'][0]
    return beta

def mk_all_beta(rdf):
    '''return list of beta for all individual stocks'''
    return [mk_beta(rdf, n) for n in range(1, 178)]

def mk_all_adf(rdf, bdf, beta_list):
    '''return alpha data frame '''
    names = range(1, len(rdf.columns) - 1)
    stock_rdf =  rdf[range(1, len(rdf.columns) - 1)]
    mkt_beta = pd.concat([beta * (rdf.market - bdf.bond) for beta in beta_list], axis=1)
    mkt_beta.columns = names
    adf = stock_rdf - bdf.bond - mkt_beta

    adf.index = [datetime.strptime(x, '%m/%d/%y') for x in rdf.date]
    return adf

def mk_all_adf_mean(all_adf, T=66):
    all_adf_mean = pd.rolling_mean(all_adf, T)
    return all_adf_mean.shift(-T + 1)


def test_all(return_df, bond_df, ar_df, start = '2005-8-1', end = '2007-7-31'):
    ''' 
    from return data, bond data and analyst rating data, compute 
    '''
    """ ar_df needs average rating for market """
    ar_df = pd.concat([ar_df.date] + [ar_df.mean(axis=1)] + [ar_df[x] for x in range(1, 178)], axis=1)
    ar_df.columns = ['date'] + range(len(ar_df.columns)-1)

    """ set timedelta for 1 Year shift """
    td = dt.timedelta(days=120)
    end_1Y = datetime.strptime(end, '%Y-%m-%d') - td
    print end_1Y
    
    """ subset datas by start and end date """
    rdf = return_df.ix[ start: end ]
    bdf = bond_df.ix[ start: end ]
    ardf_reg = ar_df.ix[ start:  str(end_1Y.year)+'-' + str(end_1Y.month) + '-' + str(end_1Y.day)]
    ardf_last = ar_df.ix[ start: end ]
    print 'last day of ardf: ', ardf_last.index[len(ardf_last) - 1]

    """ make all betas """
    beta_list = mk_all_beta(rdf)
    print '*** all securities\' beta from CAPM ***'
    print beta_list

    """ compute all alphas from betas """
    all_adf = mk_all_adf(rdf, bdf, beta_list)
    """ need to concat market return to alpha df """
    mkt = rdf.market
    all_adf = pd.concat([mkt] + [all_adf[x] for x in range(1, 178)], axis=1)
    """ for pandas 0.10 """
    all_adf.columns = [x for x in range(178)]
    print '*** all_adf.columns ***'
    print all_adf.columns
    
    """ compute 1 Year rolling mean """
    all_adf_mean = mk_all_adf_mean(all_adf, 66)

    new_all_adf_mean = pd.concat([rdf.date] + [all_adf_mean[x] for x in range(0, 178)], axis=1)
    new_all_adf_mean.columns = ['date'] + [x for x in range(0, 178)]
    new_all_adf_mean.to_csv('all_adf_mean.csv', index=False)
    sub_all_adf_mean = df_subset2(new_all_adf_mean, start, str(end_1Y.year) + '-' + str(end_1Y.month) + '-' + str(end_1Y.day))
    beta = []
    alpha = []
    print '*** regression of 1Y expected alpha and analyst ratings ***'
    y = True; n = False
    plot = input('Do you need plots?\ny/n > ')
    for i in range(0, 178):
        try:
            b, a = simple_reg(ardf_reg[i], sub_all_adf_mean[i], i, plot)
            beta.append(b)
            alpha.append(a)
        except ValueError:
            print 'ValueError on ' + str(i)
            beta.append('e'); alpha.append('e')
    return beta, alpha, ardf_last

def mk_exp_alpha(beta, alpha, ardf_last, start):
    ardf_of_simstart = ardf_last.ix[start]
    exp_alpha_list = []
    print '*** expected alphas ***'
    for i in range(0, 178):
        exp_alpha = beta[i] * ardf_of_simstart[i] + alpha[i]
        exp_alpha_list.append(exp_alpha)
        print exp_alpha
    return DataFrame(exp_alpha_list)

def main_alph_reg(return_df, bond_df, ar_df, start, end):
    print 'Compute all securities\' (1-177) beta'
    beta, alpha, ardf_last = test_all(return_df, bond_df, ar_df, start, end)
    sim_start = ardf_last.index[len(ardf_last.index) - 1]
    exp_alph_df = mk_exp_alpha(beta, alpha, ardf_last, sim_start)
    exp_alph_df.to_csv('exp_alph.csv', index=False, header=False)
  

def test(n, start = '2005-8-1', end = '2007-7-31'):
    return_df = mk_return_df('return_data.csv')
    bond_df = mk_bond_df('bond_data.csv')
    bond_df.bond = bond_df.bond / 100 / 250
    bdf = bond_df.ix[ start: end ]
    rdf = return_df.ix[ start: end ]
    beta = mk_beta(rdf, n)
    print n, 'beta: ', beta
    adf = rdf[n] - bdf.bond - beta * (rdf.market - bdf.bond)
    return adf

if __name__ == '__main__':
    test()
